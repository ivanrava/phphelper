# PHPHelper

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://gitlab.com/Y02U1/phphelper/network/master)
[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
![Website](https://img.shields.io/website-up-down-green-red/https/phphelper.herokuapp.com.svg?label=PHPHelper)

Made by Ivan _"Y02U1"_ Ravasi with 💖 (& Python)

Hosted at [PHPHelper](phphelper.herokuapp.com) (courtesy of [Heroku](heroku.com))

## Installation

1. Install Python 3.6+
2. Install Scrapy and Jinja2 (`$ pip install scrapy jinja2`)
3. Download the repo

## Usage

1. Modify the file `functions` to your liking
2. Run the spider: save the output in a .json file
3. Point `Main.py` to that .json file
4. Open `help.html`

---

##### TODO

Parametrize `Main.py`