import json
from jinja2 import Environment, FileSystemLoader


if __name__ == "__main__":
    with open("funcs.json") as json_data:
        d = json.load(json_data)
        env = Environment(loader=FileSystemLoader('templates'))
        template = env.get_template('help.html')
        total = ""
        for func in d:
            try:
                total = total + template.render(
                    title=func['title'],
                    synopsis=func['synopsis'],
                    desc=func['desc'],
                    params=func['params'],
                    returnval=func['returnval'],
                    errors=func['errors']
                )
            except KeyError:
                total = total + template.render(
                    title=func['title'],
                    desc=func['desc'],
                )

        with open("help.html", "wb") as f:
            f.write(env.get_template('final.html').render(content=total).encode('utf-8'))